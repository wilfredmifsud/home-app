import {Component, OnInit} from '@angular/core';
import { NavController } from 'ionic-angular';
import { HttpClient } from  '@angular/common/http';
import {MenuitemModel} from "../../app/menuitem.model";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {
items: Array<MenuitemModel> = [];
  constructor(public httpClient: HttpClient, public navCtrl: NavController) {

  }

  ngOnInit() {

    let itemsTmp = localStorage.getItem('items');

    if(itemsTmp) {
      this.items = JSON.parse(itemsTmp);
    }
  }

  itemSelected(link: string) {
    this.httpClient.get(link).subscribe(data => {
      console.log(data);
    })
  }

}
