import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {MenuitemModel} from "../../app/menuitem.model";

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
 items: Array<MenuitemModel> = [];
  constructor(public navCtrl: NavController) {
    let itemsTmp = localStorage.getItem('items');

    if(itemsTmp) {
      this.items = JSON.parse(itemsTmp);
    }
  }

  addItem() {
    this.items.push(new MenuitemModel());
  }

  save() {
    let jsontext = JSON.stringify(this.items);
    localStorage.setItem('items', jsontext);
    location.reload();
  }

}
